# Skupper Experiment Report - Cloud bursting your application across multiple clusters connected with Skupper

![demo video](https://gitlab.com/m-muraki/skupper-cloud-bursting-demo/-/raw/d0ac1d12df1be1efaad1657918f8a66495e41a89/files/quarkuscoffeeshop_cloud_bursting.mp4 "demo video")

## Abstract

Skupper enables the creation of virtual services that span across sites and allows you to bind various targets. Skupper also includes features such as VAN anycast and dynamic load balancing. These functionalities route traffic to one of the backends bound to the same service via the minimum cost path which is determined based on a link state routing protocol.  
  
There is a service capacity, and if the traffic exceeded the capacity, it's directed to the backend bound to the router at the site with the next lowest cost. In this article, we will leverage these features to attempt Cloud Bursting for the application.  

This experiment uses Quarkus Coffeeshop as a target of cloud bursting.  
  
The original Quarkus Coffeeshop application source can be found here, but this experiment uses a modified version that runs on Knative Eventing instead of connecting to Kafka directly to realize event-driven architecture. The application images are uploaded to Quay.io in advance, and the necessary manifests for deploying the application can be found in this repository.  

## Prerequisites

* 2 kubernetes clusters, cluster1 and cluster2
  * It requires 2 clusters because this experiment extends the same namespaces across clusters
  * One of them should be able to have ingresses

* Clone [https://gitlab.com/m-muraki/skupper-cloud-bursting-demo](https://gitlab.com/m-muraki/skupper-cloud-bursting-demo)

# Experiment

This time, it exposes the `broker-ingress` directly. If you want to run on this configuration with non-cluster-admin user, for example, exposing an `ExternalName` pointing to `broker-ingress.knative-eventing.svc.cluster.local` and referencing it instead of `broker-ingress.knative-eventing.svc.cluster.local` will achieve it.

## Install Knative components

Install Knative components following [the installation instruction](https://knative.dev/docs/install/yaml-install/eventing/install-eventing-with-yaml/).

* Switch the context to `cluster1` before you work.

### Install Knative Serving CRDs

```
$ kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.11.2/serving-crds.yaml
customresourcedefinition.apiextensions.k8s.io/certificates.networking.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/configurations.serving.knative.dev created
customresourcedefinition.apiextensions.k8s.io/clusterdomainclaims.networking.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/domainmappings.serving.knative.dev created
customresourcedefinition.apiextensions.k8s.io/ingresses.networking.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/metrics.autoscaling.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/podautoscalers.autoscaling.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/revisions.serving.knative.dev created
customresourcedefinition.apiextensions.k8s.io/routes.serving.knative.dev created
customresourcedefinition.apiextensions.k8s.io/serverlessservices.networking.internal.knative.dev created
customresourcedefinition.apiextensions.k8s.io/services.serving.knative.dev created
customresourcedefinition.apiextensions.k8s.io/images.caching.internal.knative.dev created
```

### Install Knative Serving Core

```
$ kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.11.2/serving-core.yaml
namespace/knative-serving created
role.rbac.authorization.k8s.io/knative-serving-activator created
clusterrole.rbac.authorization.k8s.io/knative-serving-activator-cluster created
clusterrole.rbac.authorization.k8s.io/knative-serving-aggregated-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/knative-serving-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/knative-serving-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-serving-namespaced-edit created
clusterrole.rbac.authorization.k8s.io/knative-serving-namespaced-view created
clusterrole.rbac.authorization.k8s.io/knative-serving-core created
clusterrole.rbac.authorization.k8s.io/knative-serving-podspecable-binding created
serviceaccount/controller created
clusterrole.rbac.authorization.k8s.io/knative-serving-admin created
clusterrolebinding.rbac.authorization.k8s.io/knative-serving-controller-admin created
clusterrolebinding.rbac.authorization.k8s.io/knative-serving-controller-addressable-resolver created
serviceaccount/activator created
rolebinding.rbac.authorization.k8s.io/knative-serving-activator created
clusterrolebinding.rbac.authorization.k8s.io/knative-serving-activator-cluster created
customresourcedefinition.apiextensions.k8s.io/images.caching.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/certificates.networking.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/configurations.serving.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/clusterdomainclaims.networking.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/domainmappings.serving.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/ingresses.networking.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/metrics.autoscaling.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/podautoscalers.autoscaling.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/revisions.serving.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/routes.serving.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/serverlessservices.networking.internal.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/services.serving.knative.dev unchanged
secret/serving-certs-ctrl-ca created
secret/knative-serving-certs created
secret/control-serving-certs created
secret/routing-serving-certs created
image.caching.internal.knative.dev/queue-proxy created
configmap/config-autoscaler created
configmap/config-defaults created
configmap/config-deployment created
configmap/config-domain created
configmap/config-features created
configmap/config-gc created
configmap/config-leader-election created
configmap/config-logging created
configmap/config-network created
configmap/config-observability created
configmap/config-tracing created
horizontalpodautoscaler.autoscaling/activator created
poddisruptionbudget.policy/activator-pdb created
deployment.apps/activator created
service/activator-service created
deployment.apps/autoscaler created
service/autoscaler created
deployment.apps/controller created
service/controller created
horizontalpodautoscaler.autoscaling/webhook created
poddisruptionbudget.policy/webhook-pdb created
deployment.apps/webhook created
service/webhook created
validatingwebhookconfiguration.admissionregistration.k8s.io/config.webhook.serving.knative.dev created
mutatingwebhookconfiguration.admissionregistration.k8s.io/webhook.serving.knative.dev created
validatingwebhookconfiguration.admissionregistration.k8s.io/validation.webhook.serving.knative.dev created
secret/webhook-certs created
```

### Install Knative Eventing CRDs

```
$ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.11.5/eventing-crds.yaml
customresourcedefinition.apiextensions.k8s.io/apiserversources.sources.knative.dev created
customresourcedefinition.apiextensions.k8s.io/brokers.eventing.knative.dev created
customresourcedefinition.apiextensions.k8s.io/channels.messaging.knative.dev created
customresourcedefinition.apiextensions.k8s.io/containersources.sources.knative.dev created
customresourcedefinition.apiextensions.k8s.io/eventtypes.eventing.knative.dev created
customresourcedefinition.apiextensions.k8s.io/parallels.flows.knative.dev created
customresourcedefinition.apiextensions.k8s.io/pingsources.sources.knative.dev created
customresourcedefinition.apiextensions.k8s.io/sequences.flows.knative.dev created
customresourcedefinition.apiextensions.k8s.io/sinkbindings.sources.knative.dev created
customresourcedefinition.apiextensions.k8s.io/subscriptions.messaging.knative.dev created
customresourcedefinition.apiextensions.k8s.io/triggers.eventing.knative.dev created
```

### Install Knative Eventing core

```
$ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.11.5/eventing-core.yaml
namespace/knative-eventing created
serviceaccount/eventing-controller created
clusterrolebinding.rbac.authorization.k8s.io/eventing-controller created
clusterrolebinding.rbac.authorization.k8s.io/eventing-controller-resolver created
clusterrolebinding.rbac.authorization.k8s.io/eventing-controller-source-observer created
clusterrolebinding.rbac.authorization.k8s.io/eventing-controller-sources-controller created
clusterrolebinding.rbac.authorization.k8s.io/eventing-controller-manipulator created
serviceaccount/pingsource-mt-adapter created
clusterrolebinding.rbac.authorization.k8s.io/knative-eventing-pingsource-mt-adapter created
serviceaccount/eventing-webhook created
clusterrolebinding.rbac.authorization.k8s.io/eventing-webhook created
rolebinding.rbac.authorization.k8s.io/eventing-webhook created
clusterrolebinding.rbac.authorization.k8s.io/eventing-webhook-resolver created
clusterrolebinding.rbac.authorization.k8s.io/eventing-webhook-podspecable-binding created
configmap/config-br-default-channel created
configmap/config-br-defaults created
configmap/default-ch-webhook created
configmap/config-ping-defaults created
configmap/config-features created
configmap/config-kreference-mapping created
configmap/config-leader-election created
configmap/config-logging created
configmap/config-observability created
configmap/config-sugar created
configmap/config-tracing created
deployment.apps/eventing-controller created
deployment.apps/pingsource-mt-adapter created
horizontalpodautoscaler.autoscaling/eventing-webhook created
poddisruptionbudget.policy/eventing-webhook created
deployment.apps/eventing-webhook created
service/eventing-webhook created
customresourcedefinition.apiextensions.k8s.io/apiserversources.sources.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/brokers.eventing.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/channels.messaging.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/containersources.sources.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/eventtypes.eventing.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/parallels.flows.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/pingsources.sources.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/sequences.flows.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/sinkbindings.sources.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/subscriptions.messaging.knative.dev unchanged
customresourcedefinition.apiextensions.k8s.io/triggers.eventing.knative.dev unchanged
clusterrole.rbac.authorization.k8s.io/addressable-resolver created
clusterrole.rbac.authorization.k8s.io/service-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/serving-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/channel-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/broker-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/flows-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/eventing-broker-filter created
clusterrole.rbac.authorization.k8s.io/eventing-broker-ingress created
clusterrole.rbac.authorization.k8s.io/eventing-config-reader created
clusterrole.rbac.authorization.k8s.io/channelable-manipulator created
clusterrole.rbac.authorization.k8s.io/meta-channelable-manipulator created
clusterrole.rbac.authorization.k8s.io/knative-eventing-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-messaging-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-flows-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-sources-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-bindings-namespaced-admin created
clusterrole.rbac.authorization.k8s.io/knative-eventing-namespaced-edit created
clusterrole.rbac.authorization.k8s.io/knative-eventing-namespaced-view created
clusterrole.rbac.authorization.k8s.io/knative-eventing-controller created
clusterrole.rbac.authorization.k8s.io/knative-eventing-pingsource-mt-adapter created
clusterrole.rbac.authorization.k8s.io/podspecable-binding created
clusterrole.rbac.authorization.k8s.io/builtin-podspecable-binding created
clusterrole.rbac.authorization.k8s.io/source-observer created
clusterrole.rbac.authorization.k8s.io/eventing-sources-source-observer created
clusterrole.rbac.authorization.k8s.io/knative-eventing-sources-controller created
clusterrole.rbac.authorization.k8s.io/knative-eventing-webhook created
role.rbac.authorization.k8s.io/knative-eventing-webhook created
validatingwebhookconfiguration.admissionregistration.k8s.io/config.webhook.eventing.knative.dev created
mutatingwebhookconfiguration.admissionregistration.k8s.io/webhook.eventing.knative.dev created
validatingwebhookconfiguration.admissionregistration.k8s.io/validation.webhook.eventing.knative.dev created
secret/eventing-webhook-certs created
mutatingwebhookconfiguration.admissionregistration.k8s.io/sinkbindings.webhook.sources.knative.dev created
```

### Install InMemory Channel and MTChannelBasedBroker for Knative Eventing

Since this demo uses the Knative Eventing MTChannelBasedBroker and InMemoryChannel as its backend, install these components.

```
$ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.11.5/in-memory-channel.yaml
serviceaccount/imc-controller created
clusterrolebinding.rbac.authorization.k8s.io/imc-controller created
rolebinding.rbac.authorization.k8s.io/imc-controller created
clusterrolebinding.rbac.authorization.k8s.io/imc-controller-resolver created
serviceaccount/imc-dispatcher created
clusterrolebinding.rbac.authorization.k8s.io/imc-dispatcher created
rolebinding.rbac.authorization.k8s.io/imc-dispatcher-tls-role-binding created
role.rbac.authorization.k8s.io/imc-dispatcher-tls-role created
configmap/config-imc-event-dispatcher created
configmap/config-observability unchanged
configmap/config-tracing unchanged
deployment.apps/imc-controller created
service/inmemorychannel-webhook created
service/imc-dispatcher created
deployment.apps/imc-dispatcher created
customresourcedefinition.apiextensions.k8s.io/inmemorychannels.messaging.knative.dev created
clusterrole.rbac.authorization.k8s.io/imc-addressable-resolver created
clusterrole.rbac.authorization.k8s.io/imc-channelable-manipulator created
clusterrole.rbac.authorization.k8s.io/imc-controller created
clusterrole.rbac.authorization.k8s.io/imc-dispatcher created
role.rbac.authorization.k8s.io/knative-inmemorychannel-webhook created
mutatingwebhookconfiguration.admissionregistration.k8s.io/inmemorychannel.eventing.knative.dev created
validatingwebhookconfiguration.admissionregistration.k8s.io/validation.inmemorychannel.eventing.knative.dev created
secret/inmemorychannel-webhook-certs created
```

```
$ kubectl apply -f https://github.com/knative/eventing/releases/download/knative-v1.11.5/mt-channel-broker.yaml
clusterrole.rbac.authorization.k8s.io/knative-eventing-mt-channel-broker-controller created
clusterrole.rbac.authorization.k8s.io/knative-eventing-mt-broker-filter created
role.rbac.authorization.k8s.io/mt-broker-filter created
serviceaccount/mt-broker-filter created
clusterrole.rbac.authorization.k8s.io/knative-eventing-mt-broker-ingress created
role.rbac.authorization.k8s.io/mt-broker-ingress created
serviceaccount/mt-broker-ingress created
clusterrolebinding.rbac.authorization.k8s.io/eventing-mt-channel-broker-controller created
clusterrolebinding.rbac.authorization.k8s.io/knative-eventing-mt-broker-filter created
rolebinding.rbac.authorization.k8s.io/mt-broker-filter created
clusterrolebinding.rbac.authorization.k8s.io/knative-eventing-mt-broker-ingress created
rolebinding.rbac.authorization.k8s.io/mt-broker-ingress created
deployment.apps/mt-broker-filter created
service/broker-filter created
deployment.apps/mt-broker-ingress created
service/broker-ingress created
deployment.apps/mt-broker-controller created
horizontalpodautoscaler.autoscaling/broker-ingress-hpa created
horizontalpodautoscaler.autoscaling/broker-filter-hpa created
```

## Deploy Quarkus Coffeeshop into the sites in the cluster1

* Switch the context to `cluster1` before you work.
* Work in `skupper-cloud-bursting-demo/cluster1/app` directory.
* Suppose that your app url (ingress host) is `www.example.com` (Replace the url to your own one).

### Create a namespace named cbcs in which the application will be running

```
$ kubectl create namespace cbcs
namespace/cbcs created
```

### Create a namespace named cbcs-db in which the database will be running

```
$ kubectl create namespace cbcs-db
namespace/cbcs-db created
```

### Create secrets and configmaps for the application and the database, and patch the Web UI manifest with your own hostname

```
$ kubectl create secret generic \
  --from-literal POSTGRES_USER=coffeeshopuser \
  --from-literal POSTGRES_PASSWORD=redhat-21 \
  secret-quarkuscoffeeshop -n cbcs

secret/secret-quarkuscoffeeshop created
```

```
$ kubectl create secret generic \
  --from-literal POSTGRES_USER=coffeeshopuser \
  --from-literal POSTGRES_PASSWORD=redhat-21 \
  secret-quarkuscoffeeshop -n cbcs-db

secret/secret-quarkuscoffeeshop created
```

```
$ kubectl create configmap \
 --from-literal CORS_ORIGINS=http://www.example.com/dashboard/ \
 --from-literal LOYALTY_STREAM_URL=http://www.example.com/dashboard/loyaltystream \
 --from-literal POSTGRES_DB=coffeeshopdb \
 --from-literal POSTGRES_URL=jdbc:postgresql://quarkuscoffeeshop-postgresql.cbcs-db.svc.cluster.local:5432/coffeeshopdb?currentSchema=coffeeshop \
 --from-literal QUARKUS_LOG_LEVEL=INFO \
 --from-literal STORE_ID=RALEIGH \
 --from-literal STREAM_URL=http://www.example.com/dashboard/stream \
 --from-literal SITE=Site1 \
 configmap-quarkuscoffeeshop -n cbcs

configmap/configmap-quarkuscoffeeshop created
```

```
$ kubectl create configmap \
 --from-literal POSTGRES_DB=coffeeshopdb \
 configmap-quarkuscoffeeshop -n cbcs-db

configmap/configmap-quarkuscoffeeshop created
```

```
$ sed -i "s/<PATCHME>/www.example.com/g" 02_quarkuscoffeeshop_web.yaml 
```

### Deploy the application

```
$ kubectl apply -f .
deployment.apps/quarkuscoffeeshop-postgresql created
service/quarkuscoffeeshop-postgresql created
deployment.apps/quarkuscoffeeshop-web created
service/quarkuscoffeeshop-web created
ingress.networking.k8s.io/quarkuscoffeeshop-web created
deployment.apps/quarkuscoffeeshop-counter created
service/quarkuscoffeeshop-counter created
deployment.apps/quarkuscoffeeshop-barista created
service/quarkuscoffeeshop-barista created
deployment.apps/quarkuscoffeeshop-kitchen created
service/quarkuscoffeeshop-kitchen created
```

### Confirm the application is deployed

```
$ kubectl get deployment -n cbcs
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
quarkuscoffeeshop-barista   3/3     3            3           2m35s
quarkuscoffeeshop-counter   3/3     3            3           2m35s
quarkuscoffeeshop-kitchen   3/3     3            3           2m35s
quarkuscoffeeshop-web       1/1     1            1           2m35s
```

```
$ kubectl get pod -n cbcs
NAME                                         READY   STATUS    RESTARTS   AGE
quarkuscoffeeshop-barista-65ff59d99f-2p8jh   1/1     Running   0          2m39s
quarkuscoffeeshop-barista-65ff59d99f-6n589   1/1     Running   0          2m39s
quarkuscoffeeshop-barista-65ff59d99f-pc2xg   1/1     Running   0          2m39s
quarkuscoffeeshop-counter-66fbdc8f5b-7q7hl   1/1     Running   0          2m39s
quarkuscoffeeshop-counter-66fbdc8f5b-rtb8s   1/1     Running   0          2m39s
quarkuscoffeeshop-counter-66fbdc8f5b-skfr6   1/1     Running   0          2m39s
quarkuscoffeeshop-kitchen-79bc8745c-2jgps    1/1     Running   0          2m39s
quarkuscoffeeshop-kitchen-79bc8745c-4wh6r    1/1     Running   0          2m39s
quarkuscoffeeshop-kitchen-79bc8745c-zsd92    1/1     Running   0          2m39s
quarkuscoffeeshop-web-9fc5675cb-2n8zk        1/1     Running   0          2m39s
```

```
$ kubectl get service -n cbcs
NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
quarkuscoffeeshop-barista   ClusterIP   172.30.144.135   <none>        80/TCP    2m42s
quarkuscoffeeshop-counter   ClusterIP   172.30.104.169   <none>        80/TCP    2m42s
quarkuscoffeeshop-kitchen   ClusterIP   172.30.33.221    <none>        80/TCP    2m42s
quarkuscoffeeshop-web       ClusterIP   172.30.250.195   <none>        80/TCP    2m42s
```

```
$ kubectl get ingress -n cbcs
NAME                    CLASS    HOSTS             ADDRESS   PORTS   AGE
quarkuscoffeeshop-web   <none>   www.example.com   ...       80      2m46s
```

```
$ kubectl get deployment -n cbcs-db
NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
quarkuscoffeeshop-postgresql   1/1     1            1           2m51s
```

```
$ kubectl get pod -n cbcs-db
NAME                                            READY   STATUS    RESTARTS   AGE
quarkuscoffeeshop-postgresql-59c9c48677-7xzcp   1/1     Running   0          2m51s
```

```
$ kubectl get service -n cbcs-db
NAME                           TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
quarkuscoffeeshop-postgresql   ClusterIP   172.30.150.218   <none>        5432/TCP   2m51s

```

## Create resources related to Knative Eventing

* Switch the context to `cluster1` before you work.
* Work in `skupper-cloud-bursting-demo/cluster1/knative` directory.

### Create a Knative Eventing MTChannelBasedBroker and triggers to deliver events to the application components

```
$ kubectl apply -f .
configmap/cbcs-mt-channel-based-broker-config created
broker.eventing.knative.dev/cbcs-mt-channel-based-broker created
trigger.eventing.knative.dev/cbcs-mt-channel-based-broker-web-web-updates created
trigger.eventing.knative.dev/cbcs-mt-channel-based-broker-counter-orders-in created
trigger.eventing.knative.dev/cbcs-mt-channel-based-broker-counter-orders-up created
trigger.eventing.knative.dev/cbcs-mt-channel-based-broker-barista-barista-in created
trigger.eventing.knative.dev/cbcs-mt-channel-based-broker-kitchen-kitchen-in created
```

### Confirm the broker and triggers are created

```
$ kubectl get broker -n cbcs
NAME                           URL                                                                                          AGE   READY   REASON
cbcs-mt-channel-based-broker   http://broker-ingress.knative-eventing.svc.cluster.local/cbcs/cbcs-mt-channel-based-broker   5s    True    
```

```
$ kubectl get trigger -n cbcs
NAME                                              BROKER                         SUBSCRIBER_URI                                                       AGE   READY   REASON
cbcs-mt-channel-based-broker-barista-barista-in   cbcs-mt-channel-based-broker   http://quarkuscoffeeshop-barista.cbcs.svc.cluster.local/barista-in   6s    True    
cbcs-mt-channel-based-broker-counter-orders-in    cbcs-mt-channel-based-broker   http://quarkuscoffeeshop-counter.cbcs.svc.cluster.local/orders-in    6s    True    
cbcs-mt-channel-based-broker-counter-orders-up    cbcs-mt-channel-based-broker   http://quarkuscoffeeshop-counter.cbcs.svc.cluster.local/orders-up    6s    True    
cbcs-mt-channel-based-broker-kitchen-kitchen-in   cbcs-mt-channel-based-broker   http://quarkuscoffeeshop-kitchen.cbcs.svc.cluster.local/kitchen-in   6s    True    
cbcs-mt-channel-based-broker-web-web-updates      cbcs-mt-channel-based-broker   http://quarkuscoffeeshop-web.cbcs.svc.cluster.local/web-updates      6s    True  
```

Up to here, the application will operate within the cluster1. Now open the Quarkus Coffeeshop web UI and try placing orders.
  
![Quarkus Coffeeshop Web UI](https://gitlab.com/m-muraki/skupper-cloud-bursting-demo/-/raw/main/files/1.png "Quarkus Coffeeshop Web UI")

  
In the following steps, we will prepare to burst this application to the other cluster.

## Deploy remote Quarkus Coffeeshop

* Switch the context to `cluster2` before you work.
* Work in `skupper-cloud-bursting-demo/cluster2/app` directory.

### Create namespaces for bursting

```
$ kubectl create namespace cbcs
namespace/cbcs created
```

```
$ kubectl create namespace cbcs-db
namespace/cbcs-db created
```

```
$ kubectl create namespace knative-eventing
namespace/knative-eventing created
```

### Create secrets and configmaps for the burst application

```
$ kubectl create secret generic \
  --from-literal POSTGRES_USER=coffeeshopuser \
  --from-literal POSTGRES_PASSWORD=redhat-21 \
  secret-quarkuscoffeeshop -n cbcs

secret/secret-quarkuscoffeeshop created
```

```
$ kubectl create configmap \
 --from-literal POSTGRES_URL=jdbc:postgresql://quarkuscoffeeshop-postgresql.cbcs-db.svc.cluster.local:5432/coffeeshopdb?currentSchema=coffeeshop \
 --from-literal QUARKUS_LOG_LEVEL=INFO \
 --from-literal SITE=Site2 \
 configmap-quarkuscoffeeshop -n cbcs

configmap/configmap-quarkuscoffeeshop created
```

### Deploy the application

```
$ kubectl apply -f .
deployment.apps/quarkuscoffeeshop-counter created
deployment.apps/quarkuscoffeeshop-barista created
deployment.apps/quarkuscoffeeshop-kitchen created
```

### Confirm the application is deployed

```
$ kubectl get all -n cbcs
NAME                                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/quarkuscoffeeshop-barista   0/0     0            0           63s
deployment.apps/quarkuscoffeeshop-counter   0/0     0            0           64s
deployment.apps/quarkuscoffeeshop-kitchen   0/0     0            0           63s

NAME                                                   DESIRED   CURRENT   READY   AGE
replicaset.apps/quarkuscoffeeshop-barista-58778897c9   0         0         0       63s
replicaset.apps/quarkuscoffeeshop-counter-6b8554bb95   0         0         0       63s
replicaset.apps/quarkuscoffeeshop-kitchen-84cc77dcc7   0         0         0       63s
```

```
$ kubectl get all -n cbcs-db
No resources found in cbcs-db namespace.
```

```
$ kubectl get all -n knative-eventing
No resources found in knative-eventing namespace.
```

### Install skupper CLI, initialize the sites in the cluster1 and create tokens for links from the sites in the cluster2 to the sites in the cluster1

* Switch the context to `cluster1` before you work.

```
$ curl https://skupper.io/install.sh | sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1742  100  1742    0     0  14396      0 --:--:-- --:--:-- --:--:-- 14516

# Determining your OS and architecture

  Operating system: linux
  Architecture: amd64

# Looking up the latest release for your environment

  https://github.com/skupperproject/skupper/releases/download/1.4.3/skupper-cli-1.4.3-linux-amd64.tgz

# Downloading and installing the Skupper command

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100 20.7M  100 20.7M    0     0  22.3M      0 --:--:-- --:--:-- --:--:-- 48.9M

# Testing the Skupper command

  Result: OK

# The Skupper command is now available:

  /home/lab-user/.local/bin/skupper
```

```
$ skupper init --enable-console --enable-flow-collector --site-name site1-cbcs -n cbcs
Skupper is now installed in namespace 'cbcs'.  Use 'skupper status' to get more information.
```

```
$ skupper token create cbcs_site2_to_site1.yaml -n cbcs
Token written to cbcs_site2_to_site1.yaml 
```

```
$ skupper init --enable-console --enable-flow-collector --site-name site1-cbcs-db --routers 10 -n cbcs-db
Skupper is now installed in namespace 'cbcs-db'.  Use 'skupper status' to get more information.
```

```
$ skupper token create cbcs-db_site2_to_site1.yaml -n cbcs-db
Token written to cbcs-db_site2_to_site1.yaml 
```

```
$ skupper init --enable-console --enable-flow-collector --site-name site1-knative-eventing --routers 10 -n knative-eventing
Skupper is now installed in namespace 'knative-eventing'.  Use 'skupper status' to get more information.
```

```
$ skupper token create knative-eventing_site2_to_site1.yaml -n knative-eventing
Token written to knative-eventing_site2_to_site1.yaml 
```

### Initialize the sites in the cluster2 and create links from the sites in the cluster2 to the sites in the cluster1

* Switch the context to `cluster2` before you work.

```
$ skupper init --enable-console --enable-flow-collector --site-name site2-cbcs --routers 10 -n cbcs
Skupper is now installed in namespace 'cbcs'.  Use 'skupper status' to get more information.
```

```
$ skupper link create cbcs_site2_to_site1.yaml -n cbcs
Site configured to link to https://claims-cbcs.apps.cluster-zrtgq.zrtgq.sandbox853.opentlc.com:443/c0d0b3c4-6d96-11ee-97a5-02845570dd13 (name=link1)
Check the status of the link using 'skupper link status'.
```

```
$ skupper init --enable-console --enable-flow-collector --site-name site2-cbcs-db --routers 10 -n cbcs-db
Skupper is now installed in namespace 'cbcs-db'.  Use 'skupper status' to get more information.
```

```
$ skupper link create cbcs-db_site2_to_site1.yaml -n cbcs-db
Site configured to link to https://claims-cbcs-db.apps.cluster-zrtgq.zrtgq.sandbox853.opentlc.com:443/f1d9dbee-6d96-11ee-afad-02845570dd13 (name=link1)
Check the status of the link using 'skupper link status'.
```

```
$ skupper init --enable-console --enable-flow-collector --site-name site2-knative-eventing --routers 10 -n knative-eventing
Skupper is now installed in namespace 'knative-eventing'.  Use 'skupper status' to get more information.
```

```
$ skupper link create knative-eventing_site2_to_site1.yaml -n knative-eventing
Site configured to link to https://claims-knative-eventing.apps.cluster-zrtgq.zrtgq.sandbox853.opentlc.com:443/313e182f-6d97-11ee-a24a-02845570dd13 (name=link1)
Check the status of the link using 'skupper link status'.
```

### Confirm the links are connected

```
$ skupper link status -n cbcs

Links created from this site:

	 Link link1 is connected
```

```
$ skupper link status -n cbcs-db

Links created from this site:

	 Link link1 is connected
```

```
$ skupper link status -n knative-eventing

Links created from this site:

	 Link link1 is connected
```

### Confirm the services haven't been exposed yet

```
$ skupper service status -n cbcs
No services defined
```

```
$ skupper service status -n cbcs-db
No services defined
```

```
$ skupper service status -n knative-eventing
No services defined
```

## Expose the services in the sites in the cluster1

* Switch the context to `cluster1` before you work.

### First, confirm the services haven't been exposed yet

```
$ skupper service status -n cbcs
No services defined
```

```
$ skupper service status -n cbcs-db
No services defined
```

```
$ skupper service status -n knative-eventing
No services defined
```

### Expose the services by giving an annotation

```
$ kubectl annotate service quarkuscoffeeshop-counter skupper.io/proxy=http -n cbcs
service/quarkuscoffeeshop-counter annotated
```

```
$ kubectl annotate service quarkuscoffeeshop-barista skupper.io/proxy=http -n cbcs
service/quarkuscoffeeshop-barista annotated
```

```
$ kubectl annotate service quarkuscoffeeshop-kitchen skupper.io/proxy=http -n cbcs
service/quarkuscoffeeshop-kitchen annotated
```

```
$ kubectl annotate service quarkuscoffeeshop-postgresql skupper.io/proxy=tcp -n cbcs-db
service/quarkuscoffeeshop-postgresql annotated
```

```
$ kubectl annotate service broker-ingress skupper.io/proxy=http -n knative-eventing
service/broker-ingress annotated
```

### Confirm the services are now exposed

```
$ skupper service status -n cbcs
Services exposed through Skupper:
├─ quarkuscoffeeshop-kitchen (http port 80)
│  ╰─ Targets:
│     ╰─ app=quarkuscoffeeshop-kitchen name=quarkuscoffeeshop-kitchen 
├─ quarkuscoffeeshop-barista (http port 80)
│  ╰─ Targets:
│     ╰─ app=quarkuscoffeeshop-barista name=quarkuscoffeeshop-barista 
╰─ quarkuscoffeeshop-counter (http port 80)
   ╰─ Targets:
      ╰─ app=quarkuscoffeeshop-counter name=quarkuscoffeeshop-counter 
```

```
$ skupper service status -n cbcs-db
Services exposed through Skupper:
╰─ quarkuscoffeeshop-postgresql (tcp port 5432)
   ╰─ Targets:
      ╰─ app=quarkuscoffeeshop-postgresql name=quarkuscoffeeshop-postgresql 
```

```
$ skupper service status -n knative-eventing
Services exposed through Skupper:
╰─ broker-ingress (http ports 80 443 9092)
   ╰─ Targets:
      ╰─ eventing.knative.dev/brokerRole=ingress name=broker-ingress 
```

## Bind the deployments to the exposed services in cbcs in the cluster2

* Switch the context to `cluster2` before you work.

### Confirm the services are now visible, and nothing are bound yet

```
$ skupper service status -n cbcs
Services exposed through Skupper:
├─ quarkuscoffeeshop-barista (http port 80)
├─ quarkuscoffeeshop-counter (http port 80)
╰─ quarkuscoffeeshop-kitchen (http port 80)
```

```
$ skupper service status -n cbcs-db
Services exposed through Skupper:
╰─ quarkuscoffeeshop-postgresql (tcp port 5432)
```

```
$ skupper service status -n knative-eventing
Services exposed through Skupper:
╰─ broker-ingress (http ports 80 443 9092)
```

### Bind the deployments to the services

```
$ skupper service bind quarkuscoffeeshop-barista deployment quarkuscoffeeshop-barista --target-port 8080 -n cbcs
```

```
$ skupper service bind quarkuscoffeeshop-kitchen deployment quarkuscoffeeshop-kitchen --target-port 8080 -n cbcs
```

```
$ skupper service bind quarkuscoffeeshop-counter deployment quarkuscoffeeshop-counter --target-port 8090 -n cbcs
```

### Confirm the deployments are bound to the services

```
$ skupper service status -n cbcs
Services exposed through Skupper:
├─ quarkuscoffeeshop-barista (http port 80)
│  ╰─ Targets:
│     ╰─ app=quarkuscoffeeshop-barista name=quarkuscoffeeshop-barista namespace=cbcs
├─ quarkuscoffeeshop-counter (http port 80)
│  ╰─ Targets:
│     ╰─ app=quarkuscoffeeshop-counter name=quarkuscoffeeshop-counter namespace=cbcs
╰─ quarkuscoffeeshop-kitchen (http port 80)
   ╰─ Targets:
      ╰─ app=quarkuscoffeeshop-kitchen name=quarkuscoffeeshop-kitchen namespace=cbcs
```

```
$ skupper service status -n cbcs-db
Services exposed through Skupper:
╰─ quarkuscoffeeshop-postgresql (tcp port 5432)
```

```
$ skupper service status -n knative-eventing
Services exposed through Skupper:
╰─ broker-ingress (http ports 80 443 9092)
```

### Scale deployments to burst in cbcs in the cluster2

* Switch the context to `cluster2` before you work.

```
$ kubectl scale deployment/quarkuscoffeeshop-counter --replicas=10 -n cbcs
deployment.apps/quarkuscoffeeshop-counter scaled
```

```
$ kubectl scale deployment/quarkuscoffeeshop-barista --replicas=20 -n cbcs
deployment.apps/quarkuscoffeeshop-barista scaled
```

```
$ kubectl scale deployment/quarkuscoffeeshop-kitchen --replicas=20 -n cbcs
deployment.apps/quarkuscoffeeshop-kitchen scaled
```

### Confirm the pods are now running

```
$ kubectl get pods -n cbcs
NAME                                         READY   STATUS    RESTARTS   AGE
quarkuscoffeeshop-barista-58778897c9-8hh7g   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-8sg82   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-dmsvx   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-fkk5d   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-l69tg   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-m2xxz   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-md7qc   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-mhsx4   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-n4rjr   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-pbx9r   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-phfkq   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-q22jl   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-qf6nj   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-r6548   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-rlpmb   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-sf4tp   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-vxlgz   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-ww7sd   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-x22lj   1/1     Running   0          24s
quarkuscoffeeshop-barista-58778897c9-xj8xv   1/1     Running   0          24s
quarkuscoffeeshop-counter-6b8554bb95-2dzrx   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-94ssf   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-9zvrl   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-c5nrg   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-cqjzc   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-klvwl   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-m6gzg   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-mxnd8   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-pbmff   1/1     Running   0          34s
quarkuscoffeeshop-counter-6b8554bb95-x9zqf   1/1     Running   0          34s
quarkuscoffeeshop-kitchen-84cc77dcc7-2rxpw   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-4x8l2   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-6pjqc   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-8h488   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-8nf2q   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-bmq7g   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-bxhgh   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-c2nxd   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-c8jt2   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-fb7lm   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-h99t5   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-hjlrt   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-j2m4j   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-j6p57   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-jzdbt   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-m8664   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-p4n4z   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-tc65g   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-v597t   1/1     Running   0          13s
quarkuscoffeeshop-kitchen-84cc77dcc7-wvk9v   1/1     Running   0          13s
skupper-prometheus-59db49845c-98pv4          1/1     Running   0          16m
skupper-router-6f59894bfd-4dmjh              2/2     Running   0          16m
skupper-router-6f59894bfd-5q8k9              2/2     Running   0          16m
skupper-router-6f59894bfd-cv2dl              2/2     Running   0          16m
skupper-router-6f59894bfd-p9j2t              2/2     Running   0          16m
skupper-router-6f59894bfd-q8qsz              2/2     Running   0          16m
skupper-router-6f59894bfd-qrlgn              2/2     Running   0          16m
skupper-router-6f59894bfd-qsktw              2/2     Running   0          16m
skupper-router-6f59894bfd-vkl4m              2/2     Running   0          16m
skupper-router-6f59894bfd-wgvrk              2/2     Running   0          16m
skupper-router-6f59894bfd-z6mkc              2/2     Running   0          16m
skupper-service-controller-95f74bc54-55kdq   2/2     Running   0          16m
```

The counter service in cbcs in cluster2 accesses DB as well as the ones in cluster1 , but just exposing the DB service from cbcs-db in the cluster1 makes it accessible in cbcs in the cluster2.  
  
Up to this point, the application has been prepared to burst to the sites in the cluster2.  
  
## Put orders to Quarkus Coffeeshop, and see how it's bursted from the sites in the cluster1 to the sites in the cluster2

* Suppose that your app url (ingress host) is `www.example.com` (Replace the url to your own one).

```
$ for x in `seq 1 100`; do curl -i -X POST -H 'Content-Type: application/json' -d '{"commandType":"PLACE_ORDER","id":"'$(uuidgen)'","orderSource":"WEB","storeId":"RALEIGH","rewardsId":"","baristaItems":[{"name":"customer'$(printf "%03d" ${x})'","item":"CAPPUCCINO","price":"3.75"}],"kitchenItems":[{"item":"CROISSANT","price":3.00,"name":"customer'$(printf "%03d" ${x})'"}]}' http://www.example.com/order; done
HTTP/1.1 202 Accepted
content-length: 286
content-type: application/json
set-cookie: 377d16b6ad7e93d35752ce1b8a45cd37=b44bc865c92313a8273a503a442108da; path=/; HttpOnly

{"commandType":"PLACE_ORDER","baristaItems":[{"item":"CAPPUCCINO","price":3.75,"name":"customer001"}],"kitchenItems":[{"item":"CROISSANT","price":3.00,"name":"customer001"}],"id":"5b7ffd8f-e37a-44b4-b6f2-91bf81fdb373","storeId":"RALEIGH","orderSource":"WEB","rewardsId":"","total":null}HTTP/1.1 202 Accepted
content-length: 286
content-type: application/json
set-cookie: 377d16b6ad7e93d35752ce1b8a45cd37=b44bc865c92313a8273a503a442108da; path=/; HttpOnly

...

{"commandType":"PLACE_ORDER","baristaItems":[{"item":"CAPPUCCINO","price":3.75,"name":"customer099"}],"kitchenItems":[{"item":"CROISSANT","price":3.00,"name":"customer099"}],"id":"cfac0925-7432-4682-abf5-95c3fe282876","storeId":"RALEIGH","orderSource":"WEB","rewardsId":"","total":null}HTTP/1.1 202 Accepted
content-length: 286
content-type: application/json
set-cookie: 377d16b6ad7e93d35752ce1b8a45cd37=b44bc865c92313a8273a503a442108da; path=/; HttpOnly

{"commandType":"PLACE_ORDER","baristaItems":[{"item":"CAPPUCCINO","price":3.75,"name":"customer100"}],"kitchenItems":[{"item":"CROISSANT","price":3.00,"name":"customer100"}],"id":"187b7e13-7a41-4293-8efc-ca7db2a09163","storeId":"RALEIGH","orderSource":"WEB","rewardsId":"","total":null}$ 
```

Then, see by which site the orders are processed in Quarkus Coffeeshop web UI.

![Quarkus Coffeeshop Web UI](https://gitlab.com/m-muraki/skupper-cloud-bursting-demo/-/raw/main/files/2.png "Quarkus Coffeeshop Web UI")


## See how the traffic is balanced in Skupper console

You can get admin password for the Skupper console like below.

```
$ kubectl get secret/skupper-console-users -o jsonpath='{.data.admin}' -n cbcs | base64 -d
haD3lRzSZz
```

![Skupper console](https://gitlab.com/m-muraki/skupper-cloud-bursting-demo/-/raw/main/files/3.png "Skupper console")

## Conclusion
Using Skupper, there is the potential to enable Cloud Bursting for an application with just a few adjustments to the manifest files. This could be one of the use cases for Skupper that might find applicability in various scenarios.  
  
It's important to be aware that accessing the database in cbcs-db in the cluster1 from applications within cbcs in the cluster2 may result in significant delays. While this configuration is sometimes discussed as a use case for Skupper, it could be more practical for temporary purposes such as bursting or migration. It could be better to manage tasks in this manner while gradually modernizing the architecture.  
  
